/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eatyogurt;

import java.util.Scanner;

/**
 *
 * @author M_rey
 */
public class EatYogurt {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Scanner scan1 = new Scanner(System.in);
        System.out.println("Enter n :");
        int n = scan.nextInt();
        System.out.println("Enter Speed :");
        String str = scan1.nextLine();
        String[] split = str.split(" ");
        int [] arr_speed = new int[n];
        for (int i = 0; i < n; i++) {
            arr_speed[i] = Integer.valueOf(split[i]);
        }
        int min = findMin(arr_speed,arr_speed.length);
        System.out.println(min);
    }
    // Function to find the minimum sum 
    public static int findMinRec(int arr[], int i, 
                                int sumCalculated, 
                                 int sumTotal) 
    { 
        // If we have reached last element. 
        // Sum of one subset is sumCalculated, 
        // sum of other subset is sumTotal- 
        // sumCalculated.  Return absolute  
        // difference of two sums. 
        if (i == 0) 
            return Math.abs((sumTotal-sumCalculated) - 
                                   sumCalculated); 
       
       
        // For every item arr[i], we have two choices 
        // (1) We do not include it first set 
        // (2) We include it in first set 
        // We return minimum of two choices 
        return Math.min(findMinRec(arr, i - 1, sumCalculated  
                                   + arr[i-1], sumTotal), 
                                 findMinRec(arr, i-1, 
                                  sumCalculated, sumTotal)); 
    } 
       
    // Returns minimum possible difference between 
    // sums of two subsets 
    public static int findMin(int arr[], int n) 
    { 
        // Compute total sum of elements 
        int sumTotal = 0; 
        for (int i = 0; i < n; i++) 
            sumTotal += arr[i]; 
       
        // Compute result using recursive function 
        return findMinRec(arr, n, 0, sumTotal); 
    } 
    
}
